<?php
	include("../includes.php");
	include(dirname(dirname(__FILE__)) . "/util/keycloak/keycloak_authorization2.php");

	class API extends REST {

		public $data = "";

		public $_id_instancia;

		public function processApi() {
		    $request_args = explode('/', rtrim($_SERVER["REQUEST_URI"], '/'));
		    $func = $request_args[5];
		    $this->_id_instancia = count($request_args) > 6 ? $request_args[6] : null;
		    if(method_exists($this, $func))
		        $this->$func();
		    else {
		        $this->response('', 404); // If the method not exist with in this class "Page not found".
		     }
		  }

	  	private function salvaPessoa() {
	  		$pessoaDO = PessoaDO::getInstance();
		    if($this->get_request_method() != "POST"){
		        $this->response('', 405);
		    }
		    if ($this->_id_instancia == null) {
		   		$result = $pessoaDO->Inserir($pessoaDO->populaPessoa($this->_request_args));
		    } else {
		   		$result = $pessoaDO->Editar($this->_id_instancia, $pessoaDO->populaPessoa($this->_request_args));
		    }
		    $this->response($result);
	  	}

	  	private function editaPessoa() {
	  		$pessoaDO = PessoaDO::getInstance();
		    if($this->get_request_method() != "PUT"){
		        $this->response('', 405);
		    }
		    if ($this->_id_instancia == null) {
		   		$result = $pessoaDO->Inserir($pessoaDO->populaPessoa($this->_request_args));
		    } else {
		   		$result = $pessoaDO->Editar($pessoaDO->populaPessoa($this->_request_args));
		    }
		    $this->response($result);
	  	}

	  	private function deletaPessoa() {
	  		$pessoaDO = PessoaDO::getInstance();
		    if($this->get_request_method() != "DELETE"){
		        $this->response('', 405);
		    }
		   	$result = $pessoaDO->Deletar($this->_id_instancia);
		    $this->response($result);
	  	}

	  	private function buscaPessoas() {
	  		$pessoaDO = PessoaDO::getInstance();
	  		$filtros = $_GET;
		    if($this->get_request_method() != "GET"){
		        $this->response('', 405);
		    }
		   	$result = $pessoaDO->Buscar($filtros);
		    $this->response($result);
	  	}

	  	private function buscaPessoa() {
	  		$pessoaDO = PessoaDO::getInstance();
		    if($this->get_request_method() != "GET"){
		        $this->response('', 405);
		    }

		    if (''.intval($this->_id_instancia) == $this->_id_instancia) {
		   		$result = $pessoaDO->BuscarPorCOD($this->_id_instancia);
		    } else {
		   		$result = $pessoaDO->BuscarPorUsername($this->_id_instancia);
		    }
		    $this->response($result);
	  	}
	}

	$api = new API();
	$api->processApi();