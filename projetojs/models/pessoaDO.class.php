<?php


class PessoaDO {

    public static $instance;

    private function __construct() {
        //
    }

    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new PessoaDO();

        return self::$instance;
    }

    public function Inserir(Pessoa $pessoa) {
        try {
            $sql = "INSERT INTO pessoa (
                nome,
                email,
                emailoficial,
                username,
                perfil)
                VALUES (
                :nome,
                :email,
                :emailoficial,
                :username,
                :perfil)";
            $conn = Conexao::getInstance();
            $p_sql = $conn->prepare($sql);
            $p_sql->bindValue(":nome", $pessoa->nome);
            $p_sql->bindValue(":username", $pessoa->username);
            $p_sql->bindValue(":email", $pessoa->email);
            $p_sql->bindValue(":emailoficial", $pessoa->emailoficial);
            $p_sql->bindValue(":perfil", $pessoa->perfil);
            $p_sql->execute();
            $rtn = $this->BuscarPorCOD($conn->lastInsertId());
            return $rtn;
        } catch (Exception $e) {
            print $e->  getMessage();
            print "Ocorreu um erro ao tentar executar esta ação de inserção, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " .
            //$e->getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function Editar(Pessoa $pessoa) {
        try {
            $sql = "UPDATE pessoa set
				nome = :nome,
                email = :email,
                emailoficial = :emailoficial,
                username = :username,
                perfil = :perfil WHERE id = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":nome", $pessoa->nome);
            $p_sql->bindValue(":email", $pessoa->email);
            $p_sql->bindValue(":emailoficial", $pessoa->emailoficial);
            $p_sql->bindValue(":username", $pessoa->username);
            $p_sql->bindValue(":perfil", $pessoa->perfil);
            $p_sql->bindValue(":id", $pessoa->id);

            $p_sql->execute();
            $rtn = $this->BuscarPorCOD($pessoa->id);
            return $rtn;
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação de edição, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->
            // getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM pessoa WHERE id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->
            //getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM pessoa WHERE id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            if ($row = $p_sql->fetch(PDO::FETCH_ASSOC)) {
                return $this->populaPessoa($row);
            } else {
                return null;
            }
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->
            //getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function BuscarPorUsername($username) {
        try {
            $sql = "SELECT * FROM pessoa WHERE username = :username";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":username", $username);
            $p_sql->execute();
            if ($row = $p_sql->fetch(PDO::FETCH_ASSOC)) {
                return $this->populaPessoa($row);
            } else {
                return null;
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->
            //getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function Buscar($filtros) {
        try {
            $sql = "SELECT * FROM pessoa";
            $p_sql = Conexao::getInstance()->prepare($sql);
            //$p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $rtns = array();
            while($row = $p_sql->fetch(PDO::FETCH_ASSOC)) {
                $rtns[] = $this->populaPessoa($p_sql->fetch(PDO::FETCH_ASSOC));
            }
            return $rtns;
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado
 um LOG do mesmo, tente novamente mais tarde.";
            //GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->
            //getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function populaPessoa($row) {
        $pojo = new Pessoa();
        $pojo->id = isset($row['id']) ? $row['id'] : null;
        $pojo->nome = isset($row['nome']) ? $row['nome'] : null;
        $pojo->email = isset($row['email']) ? $row['email'] : null;
        $pojo->username = isset($row['username']) ? $row['username'] : null;
        $pojo->emailoficial = isset($row['emailoficial']) ? $row['emailoficial'] : null;
        $pojo->perfil = isset($row['perfil']) ? $row['perfil'] : null;
        return $pojo;
    }

}

?>