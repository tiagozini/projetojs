<?php

class Pessoa {
	var $nome;
	var $username;
	var $perfil;
	var $email;
	var $emailoficial;
	var $id;

	function __construct($nome=null, $username=null, $perfil=null, $email=null, $emailoficial=null, $id=null) {
		$this->nome = $nome;
		$this->username = $username;
		$this->perfil = $perfil;
		$this->email = $email;
		$this->emailoficial = $emailoficial;
		$this->id = $id;
	}

}


