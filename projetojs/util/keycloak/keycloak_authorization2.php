<?php
    $headers = apache_request_headers();

	$token = null;
	foreach ($headers as $header => $value) {
		if ($header == "Authorization") {
			$token = substr($value, 7);
		}
	}

	//$token = $_GET[token];
    /*
    * gets url from keycloak.js, gets $usrpwd from backendconfig.json
    */
    $usrpwd = array('backend-usrpwd' =>
        constant('KC_CLIENT') .":". constant('KC_CLIENT_SECRET'),
        'realm' => constant('KC_REIGN')
    );
    $tokenCheckResult = CallAPI(constant('KC_AUTH_SERVER_URL') . "/realms/".$usrpwd['realm']."/protocol/openid-connect/token/introspect", $token, $usrpwd['backend-usrpwd']);

    if(!$tokenCheckResult) {
    	die("Usuário com acesso não autorizado!");
    }
	$tokenCheckResultEncoded = json_decode($tokenCheckResult);
    if($tokenCheckResultEncoded->active == false) {
    	die("Tempo de acesso do usuário expirou é preciso fazer login novamente!");
    } else {
    	// inicializa dados do usuário que está acessando.
    	$uDO = PessoaDO::getInstance();
    	$usuarioLogado = $uDO->BuscarPorUsername($tokenCheckResultEncoded->preferred_username);
    }