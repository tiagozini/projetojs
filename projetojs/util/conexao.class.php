<?php

class Conexao {

    public static $instance;

    private function __construct() {}

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new PDO(sprintf("mysql:host=%s;dbname=%s", constant("DB_HOST"), constant("DB_NAME")), constant("DB_USERNAME"), constant("DB_PASSWORD"),
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".constant("DB_CHARSET") ));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
        }
        return self::$instance;
    }

}

?>