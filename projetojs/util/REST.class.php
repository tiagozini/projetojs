<?php
	class REST {

	  public $_request_args = array();
	  private $_method = "";
	  public $_content_type = "application/json";
	  public $_content;
	  private $_code = 200;
	  public function __construct() {
	      $this->inputs();
	  }

	  private function inputs() {
	    $this->_method = $this->get_request_method();
	    switch($this->_method){
	        case "POST":
	            $this->_request_args = get_object_vars(json_decode(file_get_contents("php://input")));
	            break;
	        case "GET":
	            $this->_request_args = $this->cleanInputs($_GET);
	            break;
	        case "DELETE":
	        case "PUT":
	            $this->_request_args = get_object_vars(json_decode(file_get_contents("php://input")));
	            break;
	        default:
	            $this->response('Method Not Allowed',405);
	            break;
	      }
	  }

	  private function get_status_message(){
	      $status = array(
	        200 => 'OK',
	        204 => 'No Content',
	        404 => 'Not Found',
	        405 => 'Method Not Allowed',
	        406 => 'Not Acceptable',
	        500 => 'Internal Server Error');
	      return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
	  }

	  public function get_request_method(){
	      $request_method = $_SERVER['REQUEST_METHOD'];
	      if ($request_method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
	          if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
	            $request_method = 'DELETE';
	          } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
	            $request_method = 'PUT';
	          } else {
	            throw new Exception("Unexpected Header");
	          }
	      }
	      return $request_method;
	  }

	  private function cleanInputs($data){
	      $clean_input = array();
	      if (is_array($data)) {
	         foreach ($data as $k => $v) {
	            $clean_input[$k] = $this->cleanInputs($v);
	         }
	      } else {
	         if(get_magic_quotes_gpc()) {
	            $data = trim(stripslashes($data));
	         }
	         $data = strip_tags($data);
	         $clean_input = trim($data);
	      }
	      return $clean_input;
	   }

	   private function set_headers() {
	      header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
	      header("Content-Type:" . $this->_content_type);
	   }

	   public function response($data, $status = 200) {
	       $this->_code = ($status)? $status : 200;
	       $this->set_headers();
	       if (is_array($data))
	          echo json_encode($data, JSON_PRETTY_PRINT);
	       else
	          echo json_encode($data);
	       exit;
	    }
	}
