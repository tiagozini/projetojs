<html>
<head>
    <title>EstagiosSMS</title>
    <?php include("../../views/comum/comum.inc.php"); ?>
</head>
<body>
  <?php include("../../views/comum/header.inc.php"); ?>
  <?php include("../../views/comum/mensagens.inc.php"); ?>

  <!-- INICIO - conteúdo da página -->
  <h1>Cadastro de <span id="pessoa-username"></span></h1>
  <form name="frmCadastraPessoa">
  <input type="hidden" value="" name="pessoaId" id="pessoaId"/>
  <p>
    <label for="username">Username</label>
    <input value="" name="username" id="username" disabled="disabled"/>
  </p>
  <p>
    <label for="email">E-mail</label>
    <input value="" name="email" id="email" disabled="disabled"/>
  </p>
  <p>
    <label for="nome">Nome</label>
    <input value="" name="nome" id="nome" disabled="disabled" />
  </p>
  <p>
    <label for="perfil">Perfil</label>
    <input value="ALUNO" name="perfil" id="perfil-aluno" type="radio"> <label for="perfil-aluno">ALUNO</label>
    <input value="PROFESSOR" name="perfil" id="perfil-professor" type="radio"> <label for="perfil-professor">PROFESSOR</label>
  </p>
  <p>
    <label for="emailoficial">Email oficial</label>
    <input value="" name="emailoficial" id="emailoficial">
  </p>
  <p><hr/>
  </p>
    <button  type="button" onclick="cpCtrl.salvaPessoa()">Salvar</button>
  </form>
  <!-- FIM - conteúdo da página -->


  <?php include("../../views/comum/footer.inc.php"); ?>
</body>
    <?php include("../../views/comum/head.inc.php"); ?>
    <script>
      var cpCtrl = CadastraPessoaController();
      function carregouUsuarioKeycloakCustomizada() {
          cpCtrl.limpaMsgs();
          cpCtrl.carregaPessoaNoForm(pessoaLogada);
      }
    </script>
</html>