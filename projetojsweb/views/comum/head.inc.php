    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script>
      var ROOT_PATH_URL = "<?php echo constant('SITE_HOST')."/"; ?>";
    </script>
    <script src="<?php echo constant('SITE_HOST')."/"; ?>lib/jquery.rest-gh-pages/dist/1/jquery.rest.js"></script>
    <script src="<?php echo constant('SITE_HOST')."/"; ?>js/keycloak.js"></script>
    <script src="<?php echo constant('SITE_HOST')."/"; ?>js/app.js"></script>
    <script src="<?php echo constant('SITE_HOST')."/"; ?>js/pessoa/cadastraPessoaController.js"></script>
    <link rel="stylesheet" href="<?php echo constant('SITE_HOST')."/"; ?>lib/bootstrap-4.1.0/dist/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script src="<?php echo constant('SITE_HOST')."/"; ?>lib/bootstrap-4.1.0/dist/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script>
   		var pessoaClient = new $.RestClient("<?php echo constant('API_HOST') . '/rest/pessoaServiceRest.php/'; ?>", {'stringifyData' : true});
      function atualizaKeycloakHeader() {
        var keycloakHeaders = {
          ajax: {
            headers: {
              'content-type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + keycloak.token
            }
          }
        };
        pessoaClient.add('buscaPessoa', keycloakHeaders);
        pessoaClient.add('buscaPessoas', keycloakHeaders);
        pessoaClient.add('salvaPessoa', keycloakHeaders);
        pessoaClient.add('deletaPessoa', keycloakHeaders);
        pessoaClient.add('editaPessoa', keycloakHeaders);
      }
	</script>

