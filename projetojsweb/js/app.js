
function carregaDadosUsuarioNoForm(pessoaLogada)  {
    document.getElementById('nome-usuario').innerHTML =  pessoaLogada.nome;
}

var pessoaLogada = null;

function carregouUsuarioKeycloakCustomizada() {};

function carregouUsuarioKeycloak() {
    carregaDadosUsuarioNoForm(pessoaLogada);
    carregouUsuarioKeycloakCustomizada();
}

var keycloak = Keycloak(ROOT_PATH_URL + "/keycloak.json");

    var loadData = function () {

        if (keycloak.idToken) {
            atualizaKeycloakHeader();
        	var username = keycloak.idTokenParsed.preferred_username;
        	pessoaClient.buscaPessoa.read(username).done(function(pessoaCarrega) {
        		if(pessoaCarrega == null) {
        			var pessoa  = {
        				"username" : keycloak.idTokenParsed.preferred_username,
        				"email" : keycloak.idTokenParsed.email,
        				"nome" : keycloak.idTokenParsed.name,
        				"perfil" : "ALUNO",
        				"emailoficial" : null
        			}
        			pessoaClient.salvaPessoa.create(pessoa).done(function(pessoaSalva) {
        				pessoaLogada = pessoaSalva;
                        carregouUsuarioKeycloak(pessoaSalva);
        			}).fail(function(data) {
        			});
        		} else {
        			pessoaLogada = pessoaCarrega;
                    carregouUsuarioKeycloak(pessoaLogada);
        		}
        	}).fail(function(data) {

        	});
        } else {
            keycloak.loadUserProfile(function() {
        		//alert("2");
            	var username = keycloak.profile.username;
            	pessoa = pessoaClient.buscaPessoa.read(username).done(function(data) {
            		alert("Leu e voltou");
            		console.log(data);
            	}).fail(function(data) {
            		alert("Foi mas falhou");
            	});
                carregaPessoaNoForm(pessoa);
            }, function() {
                document.getElementById('profileType').innerHTML = 'Failed to retrieve user details. Please enable claims or account role';
            });
        }

    }
    var loadFailure = function () {
        document.getElementById('customers').innerHTML = '<b>Failed to load data.  Check console log</b>';
    };

    var reloadData = function () {
        keycloak.updateToken(10)
                .success(loadData)
                .error(function() {
                   // document.getElementById('customers').innerHTML = '<b>Failed to load data.  User is logged out.</b>';
                });
    };

    keycloak.init({ onLoad: 'login-required' })
        .success(reloadData)
        .error(function(errorData) {
            //document.getElementById('customers').innerHTML = '<b>Failed to load data. Error: ' + JSON.stringify(errorData) + '</b>';
        });
