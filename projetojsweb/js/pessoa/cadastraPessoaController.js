function CadastraPessoaController() {

    var controller = {};

    function limpaMsgs() {
        document.getElementById("msg-sucesso").innerHTML = '';
        document.getElementById("msg-falha").innerHTML = '';
        document.getElementById("msg-sucesso").style.display = 'none';
        document.getElementById("msg-falha").style.display = 'none';
        }

    function exibeMsgSucesso(msg) {
        var preMsg = '<strong>Sucesso!</strong>';
        var posMsg = '';//<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            //<span aria-hidden="true">&times;</span>\
        //</button>';
        document.getElementById("msg-sucesso").innerHTML = preMsg + msg + posMsg;
        document.getElementById("msg-sucesso").style.display = 'block';
    }

    function exibeMsgErro(msg) {
        var preMsg = '<strong>Erro!</strong>';
        var posMsg = '';//<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            //<span aria-hidden="true">&times;</span>\
        //</button>';
        document.getElementById('msg-falha').innerHTML = preMsg + msg + posMsg;
        document.getElementById("msg-falha").style.display = 'block';
    }

    function salvaPessoa() {
        if(validaPessoa()) {
            var pessoa = carregaPessoaDoForm();
            pessoaClient.editaPessoa.update(pessoa.id, pessoa).done(function(pessoaSalva) {
                pessoaLogada = pessoaSalva;
                exibeMsgSucesso('Cadastro salvo com sucesso!');
            }).fail(function(data) {
                exibeMsgSucesso('Não foi possível carregar tente mais tarde!');
            });
        }
    }

    function carregaPessoaNoForm(pessoaLogada) {
        document.getElementById('pessoa-username').innerHTML =  pessoaLogada.nome;
        document.getElementById('username').value = pessoaLogada.username;
        document.getElementById('email').value = pessoaLogada.email;
        document.getElementById('nome').value = pessoaLogada.nome;
        document.getElementById('pessoaId').value = pessoaLogada.id;
        document.getElementById('emailoficial').value = pessoaLogada.emailoficial;
        document.getElementById('perfil-aluno').checked = pessoaLogada.perfil == "ALUNO";
        document.getElementById('perfil-professor').checked = pessoaLogada.perfil == "PROFESSOR";
    }

    function carregaPessoaDoForm() {
        var pessoa = {};
        pessoa.username = document.getElementById('username').value;
        pessoa.nome = document.getElementById('nome').value;
        pessoa.email = document.getElementById('email').value;
        pessoa.emailoficial = document.getElementById('emailoficial').value;
        for(k in document.getElementsByName('perfil')) {
            if (document.getElementsByName('perfil')[k].checked) {
                pessoa.perfil = document.getElementsByName('perfil')[k].value;
            }
        }
        pessoa.id =  document.getElementById('pessoaId').value;
        return pessoa;
    }

    function validaPessoa() {
        /** Validar pessoa **/
        return true;
    }
    controller.salvaPessoa = salvaPessoa;
    controller.carregaPessoaNoForm = carregaPessoaNoForm;
    controller.carregaPessoaDoForm = carregaPessoaDoForm;
    controller.limpaMsgs = limpaMsgs;
    return controller;
}