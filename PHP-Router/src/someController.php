<?php
namespace PHPRouter;

final class SomeController
{
    public function usersCreate()
    {
    	return "users";
    }

    public function indexAction()
    {
    	    	return "index";
    }

    public function user()
    {
    }

    /**
     * @return mixed[]
     */
    public function page()
    {
        return func_get_args();
    }

    /**
     * @return mixed[]
     */
    public function dynamicFilterUrlMatch()
    {
        return func_get_args();
    }
}
