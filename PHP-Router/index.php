<?php
require __DIR__.'/vendor/autoload.php';

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;
use PHPRouter\SomeController;

$collection = new RouteCollection();
$collection->attachRoute(new Route('/users/', array(
    '_controller' => 'someController::usersCreate',
    'methods' => 'GET'
)));

$collection->attachRoute(new Route('/', array(
    '_controller' => 'someController::indexAction',
    'methods' => 'GET'
)));

$router = new Router($collection);
$router->setBasePath('/PHP-Router');
$route = $router->matchCurrentRequest();
